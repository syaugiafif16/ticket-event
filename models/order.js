'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.user, {
        foreignKey: 'user_id'
      })
      this.belongsTo(models.event, {
        foreignKey:'event_id'
      })
    }
  }
  order.init({
    booking_code: {
      type: DataTypes.STRING,
      allowNull:false
    },
    type: {
      type: DataTypes.STRING,
      allowNull:false
    },
    total_quantity: {
      type: DataTypes.INTEGER,
      allowNull:false
    },
    sub_total: {
      type: DataTypes.FLOAT,
      allowNull:false
    },
    total: {
      type: DataTypes.FLOAT,
      allowNull:false
    },
    billing_key: {
      type: DataTypes.STRING,
      allowNull:false
    },
    billing_type: {
      type: DataTypes.STRING,
      allowNull:false
    },
    is_paid: {
      type: DataTypes.BOOLEAN,
      allowNull:false
    },
    is_cancel:{
      type: DataTypes.BOOLEAN,
      allowNull:false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull:false
    },
    event_id: {
      type: DataTypes.INTEGER,
      allowNull:false
    }
  }, {
    sequelize,
    modelName: 'order',
    tableName: 'orders'
  });
  return orders;
};