'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.order, {
        foreignKey:'event_id'
      })
    }
  }
  event.init({
    name: {
      type: DataTypes.STRING,
      allowNull:false
    },
    description: {
      type: DataTypes.STRING,
      allowNull:false
    },
    artist_name: {
      type: DataTypes.STRING,
      allowNull:false
    },
    location: {
      type: DataTypes.STRING,
      allowNull:false
    },
    start_date: {
      type: DataTypes.DATE,
      allowNull:false
    },
    end_date: {
      type: DataTypes.DATE,
      allowNull:false
    },
    stock_quantity: {
      type: DataTypes.INTEGER,
      allowNull:false
    },
    image: {
      type: DataTypes.STRING,
      allowNull:false
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull:false
    },
    is_active:{
      type: DataTypes.BOOLEAN,
      allowNull:false
    } }, 
    {
    sequelize,
    modelName: 'event',
    tableName: 'events'
  });
  return events;
};